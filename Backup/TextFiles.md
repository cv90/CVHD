## About The Text File
The main text file in different versions:

|Version|US|JP|EU|
|---|:---:|---|---|
|Name|NPUB30505|NPJB00138|NPEB00563|
|File|textres_ja.txrc|textres_ja.txrc|textres_ja.txrc|
|File Size|96,064 Bytes|96,432 Bytes|96,064 Bytes|
|MD5 Hash|8580aae7f7e25c745bac6aa5d24c59ee|c56a5aa4363e2a0458a3bfc0178ebb57|8580aae7f7e25c745bac6aa5d24c59ee|

**Conclusion:**
US & EU used the same text, however JP didn't.
