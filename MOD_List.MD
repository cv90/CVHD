## List of completed MODs.

|#|Description|Apply to|Download|
|---:|:---|:---|:---|
|1|In-game message Chinese translation|JP: NPJB00138|https://www.romhacking.net/translations/6760/|
|2|music list bug fix|US: NPUB30505<br>JP: NPJB00138<br>EU: NPEB00563|https://www.romhacking.net/hacks/7423/|
|3|DLC12 Chinese translation|US: NPUB30505<br>JP: NPJB00138<br>EU: NPEB00563|To be released|
|4|DLC14 Chinese translation|US: NPUB30505<br>JP: NPJB00138<br>EU: NPEB00563|To be released|
|5|DLC11 Chinese translation|US: NPUB30505<br>JP: NPJB00138<br>EU: NPEB00563|https://www.romhacking.net/translations/6780/|
|6|Above #1, #3, #4, #5 integrated patch|JP: NPJB00138|https://gitlab.com/cv90/CVHD/-/blob/main/files/CVHoD_CN.exe|
|7|CH07 Trap Helper|US: NPUB30505<br>JP: NPJB00138<br>EU: NPEB00563|https://www.romhacking.net/hacks/7920|
|8|Chinese translation for base game and all DLCs|US: NPUB30505<br>EU: NPEB00563|v0.9.456 internal test build for Chinese Spring Festival 2024<br>https://gitlab.com/cv90/CVHD/-/blob/main/files/HoD_CN_v0.9.456.exe|

## 已完成的MOD列表（不定期更新）。

|编号|说明|适用版本|下载地址|
|---:|:---|:---|:---|
|1|对话交互消息汉化补丁|日版：NPJB00138|https://www.romhacking.net/translations/6760/|
|2|音乐清单重复曲目清理补丁|美版：NPUB30505<br>日版：NPJB00138<br>欧版：NPEB00563|https://www.romhacking.net/hacks/7423/|
|3|DLC12汉化补丁|美版：NPUB30505<br>日版：NPJB00138<br>欧版：NPEB00563|待发布|
|4|DLC14汉化补丁|美版：NPUB30505<br>日版：NPJB00138<br>欧版：NPEB00563|待发布|
|5|DLC11汉化补丁|美版：NPUB30505<br>日版：NPJB00138<br>欧版：NPEB00563|https://www.romhacking.net/translations/6780/|
|6|上述 #1、#3、#4、#5四合一补丁|日版：NPJB00138|https://gitlab.com/cv90/CVHD/-/blob/main/files/CVHoD_CN.exe|
|7|第七章钉板陷阱助手补丁|US: NPUB30505<br>JP: NPJB00138<br>EU: NPEB00563|https://www.romhacking.net/hacks/7920|
|8|主体加全DLC汉化补丁|US: NPUB30505<br>EU: NPEB00563|v0.9.456 2024春节Q群内测版<br>https://gitlab.com/cv90/CVHD/-/blob/main/files/HoD_CN_v0.9.456.exe|


## 常用xdelta补丁工具下载：
1. xdelta UI    https://www.romhacking.net/utilities/598/
2. Delta Patcher   https://www.romhacking.net/utilities/704/
